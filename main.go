package main
import (
	"net/http"
	"fmt"
	"encoding/base64"
	"io/ioutil"
	"bytes"
	"encoding/json"
	"dataCovid/dto"
	"dataCovid/mappers"
	"go.mongodb.org/mongo-driver/mongo"
    "go.mongodb.org/mongo-driver/mongo/options"
	"context"
	"time"
	"sync"

)


func getConnection() (*mongo.Collection, *mongo.Client, context.Context){
	
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI("mongodb://root:rootpassword@localhost:27017/mydb1?authSource=admin&readPreference=primary&appname=MongoDB%20Compass&ssl=false"))
	if(err != nil){
		panic(err)
	}
	//defer client.Disconnect(ctx)
	database := client.Database("testGolang")
	vacinaCol := database.Collection("vacinaCol")
	fmt.Println("Conectado com o bd")
	return vacinaCol, client, ctx
}



func basicAuth() string {
	var username string = "imunizacao_public"
	var password string = "qlto5t&7r_@+#Tlstigi"

	auth := username + ":" + password
	return base64.StdEncoding.EncodeToString([]byte(auth))
}

func insertVac(vac *dto.Hit2, conn *mongo.Collection, client *mongo.Client) {
	
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	document := mappers.ResponseToDatabase(vac)
	_, err := conn.InsertOne(ctx, document)
	if(err != nil){
		panic(err)
	}
	



}

func firstRequest(client *http.Client, conn *mongo.Collection, clientDB *mongo.Client)   {
	var url string = "https://imunizacao-es.saude.gov.br/_search?scroll=1m"
	
	var jsonStr = []byte(`{"size": 10000}`)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	req.Header.Add("Authorization", "Basic " + basicAuth())
    req.Header.Set("Content-Type", "application/json")
	resp, err := client.Do(req)
	if(err != nil){fmt.Println("ERRO")}	
	body, _ := ioutil.ReadAll(resp.Body)
	var dataResponse dto.ReturnBody
	r := bytes.NewReader(body)
	chatErr := json.NewDecoder(r).Decode(&dataResponse)
	if(chatErr != nil) {panic(chatErr)}
	fmt.Println(dataResponse.Scroll)
	fmt.Println(dataResponse.Hits.Hit2[0].Source.VacinaDataAplicacao)
	
	
	for _, hit := range dataResponse.Hits.Hit2{

		go insertVac(hit, conn, clientDB)
	}
	restOfScrolls(client, 0, dataResponse.Scroll, conn, clientDB)
	fmt.Println("Sai do principal")
	return 
}



func restOfScrolls(client *http.Client, numOfTries int, scroll string,
	 conn *mongo.Collection, clientDB *mongo.Client){
	fmt.Println("Tries: ", numOfTries)
	if(scroll==""){
		fmt.Println("aqui?")
		return
	}
	var url string = "https://imunizacao-es.saude.gov.br/_search/scroll"
	var jsonStr = []byte(`{"scroll_id": "` + scroll + `", "scroll": "1m"}`)
	fmt.Println(string(jsonStr))
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	if(err != nil){fmt.Println("erro aqui bro")}
	req.Header.Add("Authorization", "Basic " + basicAuth())
	req.Header.Set("Content-Type", "application/json")
	resp, err := client.Do(req)
	if(err != nil){fmt.Println("ERRO")}	
	body, _ := ioutil.ReadAll(resp.Body)
	var dataResponse dto.ReturnBody
	r := bytes.NewReader(body)
	chatErr := json.NewDecoder(r).Decode(&dataResponse)
	if(chatErr != nil) {panic(chatErr)}
	fmt.Println("scroll", dataResponse.Scroll)
	
	for _, hit := range dataResponse.Hits.Hit2{

		go insertVac(hit, conn, clientDB)
	}
	restOfScrolls(client, numOfTries+1, dataResponse.Scroll, conn, clientDB)
	fmt.Println("Sai do %d",numOfTries)
	return
	
	
	
}

func main() {
	var wg sync.WaitGroup
	conn, clientDB, _ := getConnection()
	fmt.Println("Começando")
	client := &http.Client{}
	firstRequest(client, conn, clientDB)
	wg.Wait()
	//defer clientDB.Disconnect(ctx)
	
	
}