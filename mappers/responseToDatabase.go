package mappers

import (
	"dataCovid/dto"
	"go.mongodb.org/mongo-driver/bson/primitive"

)

func ResponseToDatabase (hit *dto.Hit2 ) dto.Document { 

	var document dto.Document

	primitiveId, _ := primitive.ObjectIDFromHex(hit.Id)
	document.ID = primitiveId
	document.DescDose = hit.Source.VacinaDescricaoDose
	document.DataAplicacao = hit.Source.VacinaDataAplicacao
	return document
}