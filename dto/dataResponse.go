package dto
type shard struct {

	Total int64 `json:total`
	Successful int64 `json:successful`
	Skipped int64 `json:skipped`
	Failed int64 `json:failed`
}

type total struct {
	Value int64 `json:value`
	Relation string `json:relation`
}

type source struct {

	VacinaDataAplicacao string `json:"vacina_dataAplicacao"`
	PacienteRacaCorCodigo string `json:"paciente_racaCor_codigo"`
	PacienteRacaCorValor string `json:"paciente_racaCor_valor"`
	PacienteEnderecoCoIbgeMunicipio string `json:"paciente_endereco_coIbgeMunicipio"`
	VacinaFabricanteNome string `json:"vacina_fabricante_nome"`
	EstabelecimentoNoFantasia string `json:"estalecimento_noFantasia"`
	PacienteIdade int64 `json:"paciente_idade"`
	PacienteEnderecoCoPais string `json:"paciente_endereco_coPais"`
	PacienteDataNascimento string  `json:"paciente_dataNascimento"`
	DocumentId string `json:"document_id"`
	EstabelecimentoUF string `json:"estabelecimento_uf"`
	VacinaDescricaoDose string `json:"vacina_descricao_dose"`
	PacienteEnderecoUf string `json:"paciente_endereco_uf"`
	EstabelecimentoMunicipioNome string `json:"estabelecimento_municipio_nome"`
	PacienteEnderecoCep string `json:"paciente_endereco_cep"`
	Timestamp string `json:"@timestamp"`
	PacienteEnumSexoBiologico string `json:"paciente_nacionalidade_enumNacionalidade"`
	SistemaOrigem string `json:"sistema_origem"`
	EstabelecimentoValor string `json:"estabelecimento_valor"`
	PacienteEndereconmMunicipio string `json:"paciente_endereco_nmPais"`
	EstabelecimentoRazaoSocial string `json:"estabelecimento_valor"`
	PacienteEnderecoNmMunicipio string `json:"paciente_endereco_nmMunicipio"`
	VacinaGrupoAtendimentoCodigo string `json:"vacina_grupoAtendimento_codigo"`
	VacinaLote string `json:"vacina_lote"`
	VacinaCodigo string `json:"vacina_codigo"`
	VacinaNome string `json:"vacina_nome"`
	DataImportacaoRNDS string `json:"data_importacao_rnds"`
	Version string `json:"@version"`


}

type Hit2 struct {

	Index string `json:"_index"`
	Type string `json:"_type"`
	Id string `json:"_id"`
	Score float32`json:"_score"`
	Source *source `json:"_source"`
	


}

type hit1 struct {

	Total *total `json:total`
	Max_score float32 `json:max_score`
	Hit2 []*Hit2 `json:"hits"`
 	
}


type ReturnBody struct {

	Scroll string `json:"_scroll_id"`
	Took int64 `json:took`
	Timed_out bool `json:timed_out`
	Shards *shard `json:_shards`
	Hits *hit1 `json:hits`

}